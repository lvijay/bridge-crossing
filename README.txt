ASSUMPTIONS AND DESIGN DECISIONS
--------------------------------

It is assumed that there's a bridge which can accommodate n cars at a
time.  The bridge goes from East to West so all Cars have a boolean
property named 'eastBound' which indicates if the car is moving to the
east or west.

The bridge/car analogy isn't perfect.  Though cars can travel only in
one direction at a time (no collisions on the bridge), it is possible
for a car c2 to enter the bridge after car c1 but leave the bridge
before c1.

All cars have 3 properties:

* bridge
* direction of travel
* time to cross bridge

RUNNING
-------

The provided jar can be executed as

$ java -jar bridge.jar bridgeCapacity [dir time] pairs

[dir time] are expected to be in the format <direction> <travelTime>...

where

    direction is one of [e, w].  It indicates the car's travel
    direction.

    travelTime is a long.  It represents the time it takes this car to
    cross the bridge.

For example, the input

3 e 100 w 1000 e 200

is interpreted as:

The bridge has capacity 3; there's one eastbound car that takes 100ms,
a westbound car that takes 1s, and another eastbound car that takes
200ms.

OUTPUT
------

As output, the program prints the time taken for all the cars to cross
the bridge.

VERBOSE OUTPUT
--------------

To get a verbose output that shows how the cars cross the bridge,
provide the argument -verbose as the last argument.

SOURCE
------

The jar contains the source code and .class files

Bridge.java -- represents the bridge object

Car.java -- represents the car object.  This is the main class and has
            all the decision making for crossing the bridge.

NOTES ON THE CODE
-----------------

The code is written for clarity, efficiency and brevity (in that
order).  In the ideal world, Car.java will be Runnable, take Bridge
and eastBound as parameters.  Bridge.java won't have all public
members and so on.
