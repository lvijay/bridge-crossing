package carbridge;

import java.util.logging.Level;

import carbridge.utils.Logger;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Logger.getInstance().logLevel = Level.OFF;
        Logger.getInstance().start();

        StringBuilder carsAndDurations = new StringBuilder();
        int capacity;
        try {
            capacity = Integer.parseInt(args[0]);

            for (int i = 1; i < args.length - 1; i += 2) {
                String dir = args[i];
                String time = args[i + 1];

                if (!("E".equalsIgnoreCase(dir)
                        || "W".equalsIgnoreCase(dir)))
                {
                    throw new IllegalArgumentException("Direction must be one of [e, w] got '"
                            + dir + "'");
                }

                try {
                    Long.parseLong(time);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("time must be a number, got '"
                            + time + "'");
                }

                carsAndDurations.append(dir).append(' ').append(time).append(' ');
            }

            if ("-verbose".equals(args[args.length - 1])) {
                Logger.getInstance().logLevel = Level.ALL;
            }
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println("args: bridgeCapacity [dir time] pairs");
            System.exit(1);
            throw new RuntimeException("unreachable");
        }

        carsAndDurations.deleteCharAt(carsAndDurations.length() - 1);

        Simulate.simulate(capacity, carsAndDurations.toString());
    }
}
