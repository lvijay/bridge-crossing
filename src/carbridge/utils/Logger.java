package carbridge.utils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

public class Logger extends Thread {
    private static final class Pair {
        public final Level level;
        public final String message;
        public Pair(Level level, String message) {
            this.level = level;
            this.message = message;
        }
    }

    private static Logger instance = new Logger();

    public static Logger getInstance() {
        return instance;
    }

    private final LinkedBlockingQueue<Pair> messages = new LinkedBlockingQueue<>();
    public volatile Level logLevel = Level.OFF;

    @Override
    public void start() {
        setDaemon(true);
        super.start();
    }

    @Override
    public void run() {
        try {
            while (!isInterrupted()) {
                log(messages.take());
            }
        } catch (InterruptedException e) {
            interrupt();
        }
    }

    public void info(String message) {
        message = Thread.currentThread().getName() + ": " + message;
        messages.offer(new Pair(Level.INFO, message));
    }

    public void flush() {
        Pair msg;
        while ((msg = messages.poll()) != null) {
            log(msg);
        }
    }

    private void log(Pair msg) {
        if (msg.level.intValue() >= logLevel.intValue()) {
            System.out.println(msg.message);
        }
    }
}
