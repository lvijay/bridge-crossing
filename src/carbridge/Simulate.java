package carbridge;

import java.util.concurrent.TimeUnit;

public class Simulate {
    public static long simulate(int capacity, String carsAndDurations)
            throws InterruptedException
    {
        Car[] cars = cars(capacity, carsAndDurations.split("\\s+"));
        long actualTime = getActualTime(cars);

        System.out.printf("timeTaken=%d%n", actualTime);

        return actualTime;
    }

    private static long getActualTime(Car... cars)
            throws InterruptedException
    {
        long start = System.nanoTime();
        for (Car car : cars) { car.start(); }
        for (Car car : cars) { car.join(); }
        long end = System.nanoTime();
        long actualTime = TimeUnit.NANOSECONDS.toMillis(end - start);

        return actualTime;
    }

    private static Car[] cars(int capacity, String... carsAndDurations) {
        Bridge b = new Bridge(capacity);
        Car[] cars = new Car[carsAndDurations.length / 2];

        for (int i = 0; i < carsAndDurations.length; i += 2) {
            String east = carsAndDurations[i];
            long travelTime = Long.parseLong(carsAndDurations[i+1]);
            boolean eastBound = "E".equalsIgnoreCase(east);
            Car car = new Car(b, eastBound, travelTime);
            cars[i/2] = car;
        }

        return cars;
    }
}
