package carbridge.test;

import java.util.logging.Level;

import carbridge.Simulate;
import carbridge.utils.Logger;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Logger.getInstance().start();
        Logger.getInstance().logLevel = Level.OFF;

        long actual;

        actual = Simulate.simulate(3, "e 100 e 100 w 100 e 100 w 100 w 100 e 100");
        assertTimes(300, actual, 10.0d);
        actual = Simulate.simulate(3, "e 400 e 100 w 500 e 100 w 500 w 500 e 100");
        assertTimes(1000, actual, 2.0d);
        actual = Simulate.simulate(3, "e 400 e 100 e 100 e 100 w 500 w 500 e 100");
        assertTimes(1000, actual, 2.0d);
        actual = Simulate.simulate(3, "e 900 e 800 e 700 e 600 e 500 e 400 e 300 e 200 e 100");
        assertTimes(1600, actual, 2.0d);
        actual = Simulate.simulate(3, "e 100 e 200 e 300 e 400 e 500 e 600 e 700 e 800 e 900");
        assertTimes(1700, actual, 2.0d);
    }

    private static void assertTimes(long expected, long actual, double threshold) {
        if (actual < expected) {
            throw new IllegalStateException("finished too quickly");
        }

        double errorPercentage = (actual - expected) * 100.0d / actual;

        if (errorPercentage > threshold) {
            throw new IllegalStateException("too slow");
        }
    }
}
