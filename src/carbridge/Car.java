package carbridge;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;

import carbridge.utils.Logger;

public class Car extends Thread {
    private static final AtomicInteger ecounter = new AtomicInteger(100);
    private static final AtomicInteger wcounter = new AtomicInteger(100);
    private static final Logger logger = Logger.getInstance();

    public final Bridge bridge; // for testing
    public final boolean eastBound;
    public final long travelTimeMillis;

    public Car(Bridge bridge, boolean eastBound, long travelTimeMillis) {
        super(eastBound
                ? "E" + ecounter.incrementAndGet()
                : "W" + wcounter.incrementAndGet());

        this.bridge = bridge;
        this.eastBound = eastBound;
        this.travelTimeMillis = travelTimeMillis;
    }

    @Override
    public void run() {
        try {
            crossBridge();
        } catch (InterruptedException e) {
            interrupt();
        }
    }

    private void crossBridge() throws InterruptedException {
        bridge.register(this);

        Condition shouldWait = bridge.getCondition(eastBound);
        bridge.lock.lock();
        try {
            while (true) {
                // reasons to wait
                // (1) there are cars on the bridge
                //     moving towards me.
                if (bridge.counter > 0
                        && bridge.direction != eastBound)
                {
                    shouldWait.await();
                    continue;
                }

                // (2) current direction is mine but
                // (2.1) the bridge is full or
                // (2.2) cars are waiting on the other side and
                //       my side has already used the bridge fully
                if (bridge.direction == eastBound) {
                    if (bridge.counter == bridge.capacity) {
                        shouldWait.await();
                        continue;
                    } else if (bridge.isWaiting(!eastBound)
                            && bridge.traveled >= bridge.capacity) {
                        shouldWait.await();
                        continue;
                    }
                }

                break;
            }

            bridge.unregister(this);

            if (bridge.direction != eastBound) {
                bridge.traveled = 0;
            }

            bridge.direction = eastBound;
            bridge.counter++;
            bridge.traveled++;

            log(true);

            signalAppropriateSide();
        } finally {
            bridge.lock.unlock();
        }

        travel();

        bridge.lock.lock();
        try {
            bridge.counter--;
            signalAppropriateSide();

            log(false);
        } finally {
            bridge.lock.unlock();
        }
    }

    private void signalAppropriateSide() {
        Condition signal;
        Condition signalMySide = bridge.getCondition(eastBound);
        Condition signalTheirSide = bridge.getCondition(!eastBound);
        boolean mySideWaiting = bridge.isWaiting(eastBound);
        boolean thatSideWaiting = bridge.isWaiting(!eastBound);

        if (thatSideWaiting) {
            if (bridge.traveled >= bridge.capacity) {
                // no more from my side
                signal = signalTheirSide;
            } else if (mySideWaiting) {
                // there are cars on my side, let them pass
                // until bridge capacity reached
                signal = signalMySide;
            } else {
                // there's nobody waiting on my side
                // signal the other side
                signal = signalTheirSide;
            }
        } else { // nobody waiting on the other side
            if (mySideWaiting) {
                // signal just my side
                signal = signalMySide;
            } else {
                // nobody on my side either
                signal = null;
            }
        }

        if (signal != null) {
            signal.signal();
        }
    }

    private void log(boolean entering) {
        if (logger.logLevel.intValue() > java.util.logging.Level.INFO.intValue()) {
            return;
        }
        String enter = entering ? "enter" : "exit ";
        String me_waiting = bridge.isWaiting(eastBound)  ? "T" : "F";
        String he_waiting = bridge.isWaiting(!eastBound) ? "T" : "F";
        me_waiting = String.format("%s(%s)",
                eastBound ? "E" : "W",
                me_waiting);
        he_waiting = String.format("%s(%s)",
                !eastBound ? "E" : "W",
                he_waiting);
        String waiting = eastBound
                ? me_waiting + ", " + he_waiting
                        : he_waiting + ", " + me_waiting;

        String message = String.format("%s onBridge=%d waiting=%s traveled=%d",
                enter,
                bridge.counter,
                waiting,
                bridge.traveled);

        logger.info(message);
    }

    /**
     * Actually travel across the bridge.
     *
     * @throws InterruptedException
     */
    protected void travel() throws InterruptedException {
        Thread.sleep(travelTimeMillis);
    }
}
