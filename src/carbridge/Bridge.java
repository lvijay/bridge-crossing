package carbridge;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bridge {
    public final int capacity;
    public final Lock lock = new ReentrantLock(true);
    private final Condition[] conditions = new Condition[] {
            lock.newCondition(),
            lock.newCondition()
        };
    private final Lock counterLock = new ReentrantLock(); // fairness overkill
    private final int[] sideCounter = new int[] { 0, 0 };
    public volatile int counter = 0;
    public volatile int traveled = 0;
    public volatile boolean direction;

    public Bridge(int capacity) {
        this.capacity = capacity;
    }

    public void register(Car car) {
        try {
            counterLock.lock();
            sideCounter[idx(car.eastBound)]++;
        } finally {
            counterLock.unlock();
        }
    }

    public void unregister(Car car) {
        try {
            counterLock.lock();
            sideCounter[idx(car.eastBound)]--;
        } finally {
            counterLock.unlock();
        }
    }

    public Condition getCondition(boolean eastBound) {
        return conditions[idx(eastBound)];
    }

    public boolean isWaiting(boolean eastBound) {
        return sideCounter[idx(eastBound)] > 0;
    }

    private int idx(boolean eastBound) {
        return eastBound ? 0 : 1;
    }
}
